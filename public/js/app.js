
$(document).ready(function(){
    console.log("My Kit is ready :)");
    // js for footer-responsive
    $(".item").on('click', function() {
        if ($(window).width() < 992) {
            $(this).find("ul:first").toggle(1);
            $($(this).find("a")).css("color","rgba(255,255,255,.5)");
            $($(this).find('i')).toggleClass("fas fa-chevron-down fas fa-chevron-up"); 
        }
    });
    $(window).resize(function() {
        if( $(this).width() > 992 ) {
            $(".item").find("ul:first").show();
            $($(".footer-main-top").find("i")).hide();
            $($(".item").find("a")).css("color","#f0f0f0");
        }
        else{
            $(".item").find("ul:first").hide();
            $($(".item").find("i")).show();
            $($(".footer-main-top").find("i")).removeClass('fas fa-chevron-up').addClass('fas fa-chevron-down');
        }
    });
    // js for header-responsive
    $("#menu-down").on('click', function() {
        $('.main-nav-menu').toggle(1);
        $(this).toggleClass("fas fa-chevron-down fas fa-chevron-up"); 
    });
    // js for page detail-product
    $('#display-form-evaluation').on('click', function() {
        $('.hide-dt').show();
        return false
    });
    $('#delete-form').on('click', function() {
        $('.hide-dt').hide();
        return false
    });
    //js tang giam so luong san pham
    $('.tang').on('click', function() {
        var soluong = $($(this).parent()).find('.soluong').html();
        +soluong++;
        $($(this).parent()).find('.soluong').text(' '+ soluong +' ');
    });

    $('.giam').on('click', function() {
        var soluong = $($(this).parent()).find('.soluong').html();
        +soluong--;
        $($(this).parent()).find('.soluong').text(' '+ soluong +' ');
        if(soluong<=1){
            soluong=1;
            $($(this).parent()).find('.soluong').text(' '+ soluong +' ');
        }
    });
    //js display product for page đetail-product
    $('#sp_chinh_min').on('click', function() {
        $("#sp_chinh_max").css("display", "block");
        $("#sp_phu1_max").css("display", "none");
        $("#sp_phu2_max").css("display", "none");
    });
    $('#sp_phu1_min').on('click', function() {
        $("#sp_chinh_max").css("display", "none");
        $("#sp_phu1_max").css("display", "block");
        $("#sp_phu2_max").css("display", "none");
    });
    $('#sp_phu2_min').on('click', function() {
        $("#sp_chinh_max").css("display", "none");
        $("#sp_phu1_max").css("display", "none");
        $("#sp_phu2_max").css("display", "block");
    });
    //js for menu header 
    $('#menu-item-sp,#nav-down-sp').hover(function() {
        $('.nav-down-sp').show();
        $('.main').css('background','rgba(32, 32,32,0.4)');
        $('.main').css('filter','blur(3px)');
    },function(){
        $('.nav-down-sp').hide();
        $('.main').css('background','white');
        $('.main').css('filter','blur(0px)');
    });
    // js for header-responsive
    $('#item-detail-sp').on('click', function() {
        $($(this).parent()).find("ul:first").toggle(1);
        $($(this).find('i')).toggleClass("fas fa-chevron-down fas fa-chevron-up"); 
    });
    $('#item-mask-sp').on('click', function() {
        $($(this).parent()).find("ul:first").toggle(1);
        $($(this).find('i')).toggleClass("fas fa-chevron-down fas fa-chevron-up"); 
    });
    $('#item-cleaners-sp').on('click', function() {
        $($(this).parent()).find("ul:first").toggle(1);
        $($(this).find('i')).toggleClass("fas fa-chevron-down fas fa-chevron-up"); 
    });
    $('#item-moisturisers-sp').on('click', function() {
        $($(this).parent()).find("ul:first").toggle(1);
        $($(this).find('i')).toggleClass("fas fa-chevron-down fas fa-chevron-up"); 
    });
    //tag my-account
    $(".tab_content").hide();
    $(".tab_content:first").show();
    $(".tab_history").hide();
    $(".tab_history:eq(1)").show();
    $(".tab_points").hide();
    $(".tab_points:eq(2)").show();
    $(".tab_history_comments").hide();
    $(".tab_history_comments:eq(3)").show();

    $("ul.tabs li").click(function() {  
        $(".tab_content").hide();
        $(".tab_history").hide();
        $(".tab_points").hide();
        $(".tab_history_comments").hide();
        var activeTab = $(this).attr("rel"); 
        $("#"+activeTab).fadeIn();                  
        $("ul.tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab_drawer_heading").removeClass("d_active");
        $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");  
    });
    $(".tab_drawer_heading").click(function() {
        $(".tab_content").hide();
        $(".tab_history").hide();
        $(".tab_points").hide();
        $(".tab_history_comments").hide();
        var d_activeTab = $(this).attr("rel"); 
        $("#"+d_activeTab).fadeIn();
        $(".tab_drawer_heading").removeClass("d_active");
        $(this).addClass("d_active");
        $("ul.tabs li").removeClass("active");
        $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
    $('ul.tabs li').last().addClass("tab_last");
    //reset ma xac nhan checkone
    $('.reset_icon').on('click', function() {
        $('#load')[0].reset();
    });
    // update post
    $("#submit").click(function(){
        var data = {
            id: $("#id").val(),
            title:$("#title").val(),
            body:tinymce.get("body").getContent()
        }
        $.ajax({
            url: location.protocol + "//"+document.domain+":" +location.port +"/post/update",
            method: "PUT",
            data: data,
            dataType:"json",
            success: function(result){
                console.log(result);
                if(result && result.status_code == 200){
                    console.log('213')
                     window.location.assign("/posts");
                }  
            }
        }); 
    });
});

//soket.io in chatroom
