var mysql = require('mysql');
var config = require("./../config");

// console.log(config.server_port);
var con = mysql.createConnection({
  host: config.database_mysql.host,
  user: config.database_mysql.user,
  password: config.database_mysql.password,
  database: config.database_mysql.database
});
con.connect(function(err){
	if(err){
		console.log(err);
	}
	return;
});

module.exports = con;