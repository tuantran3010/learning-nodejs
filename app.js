var express = require("express");
var engine = require('ejs-locals');
var config = require("./config");
var bodyParser = require("body-parser")
var cookieParser = require('cookie-parser')
var app = express();
var router = require(__dirname+"/router");
var chatroom = require("./controller/chatroom/chatroom")
//use bodyparser
app.use(bodyParser.json());
app.use(cookieParser())
app.use(express.urlencoded({ extended: true }));
//use router
app.use(router);
//cau hinh ejs-locals
app.engine('ejs', engine);
//cau hnh ejs
app.set("view engine","ejs");
app.set("views","./views")
//chon dương dan
app.use("/static",express.static(__dirname + '/public'));
// app.use("/post/edit/static",express.static(__dirname + '/public'));
//chon cong ket noi


var server = app.listen(3080);
var io = require('socket.io')(server);

chatroom.chat(io);

// app.listen(config.server_port);
// //route
// app.get("/home/:id",function(req,res){
// 	var i = req.params.id;
// 	res.render("home",{id:["1990","1991","1992"]});
// });
// app.get("/test/:id",function(req,res){
// 	var i = req.params.id;
// 	res.send("xuat ra bien id="+i);
// });
// app.get("/home",function(req,res){
// 	res.render("home");
// });

