var bcrypt = require('bcryptjs')
var config = require("./../../config");

module.exports.hash_password = function(password){
	var salt = bcrypt.genSaltSync(config.salt);
	var hash = bcrypt.hashSync(password, salt);

	return hash;
}

module.exports.comparePassword = function(password,hash){
	return bcrypt.compareSync(password, hash);
}