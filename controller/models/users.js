var db = require('./../../mysql/mysql');
var helper = require('./../helper/hashPassword')

module.exports.create = function(user){
	var user = {
		email:user.email,
		password:helper.hash_password(user.password)
	};
	db.connect(function(err) {
	    if (err) throw err;
	    console.log("Connected!");
	    var sql = "INSERT INTO user SET ?";
		db.query(sql ,user,function (err, result) {
		  	if (err) throw err;
		    console.log("Database created");
		});
	});
}