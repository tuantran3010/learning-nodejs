var express = require("express");
var q = require("q");
var db = require('./../../mysql/mysql');
var users = require('./../models/users');
var db = require('./../../mysql/mysql');
var helper = require('./../helper/hashPassword');
var router = express.Router();

module.exports.show = function(req,res,next){
	var sql = "SELECT * FROM posters"
    db.query(sql ,function (err, result) {
    	if(err) throw err;
    	else{
    		res.render("pages/posts",{posts:result});
    	}
    });
}
module.exports.create = function(req,res,next){
	console.log(req.body);
	post = req.body;
	var now = new Date();
	post.created_at = now;
	post.updated_at =now;
	var sql = "INSERT INTO posters SET ?"
    db.query(sql ,post ,function (err, result) {
    	if(err) throw err;
    	else{
    		console.log(result);
    		res.redirect("/posts");
    	}
    });
}
module.exports.edit = function(req,res,next){
    var id = req.params.id;
    var sql = "SELECT * FROM posters WHERE ?"
    db.query(sql ,{id:id} ,function (err, result) {
        if(err) throw err;
        else{
            // res.sendFile('edit/id', {root: './public'});
            res.render("pages/edit_post",{data:result});

        }
    });
}
module.exports.update = function(req,res,next){
    var data = req.body;
    var sql = 'UPDATE posters SET title = ?,body = ? WHERE id = ?'
    db.query(sql ,[data.title,data.body,data.id] ,function (err, result) {
        if(err) throw err;
        else{
            res.json({status_code:200});
        }
    });
}