var express = require("express");
var q = require("q");
var db = require('./../../mysql/mysql');
var users = require('./../models/users');
var helper = require('./../helper/hashPassword');
var router = express.Router();
var Token = require('./../../auth/token');

module.exports.validateRegister = function(req,res,next){
	var user =req.body;
	// console.log(user.password != user.repassword);
	if(user.email.trim().length==0)
	{
		res.render("pages/register",{data : {error:"Ban chua nhap email"}});
		// console.log(data.error)
	}
	else if(user.password.trim().length==0)
	{
		res.render("pages/register",{data : {pw:"Ban chua nhap pw"}});
		// console.log(data.error)
	}
	else if(user.password!=user.repassword)
	{
		res.render("pages/register",{data : {repw:"pw nhap lai khong trung voi pw"}});
		// console.log(data.error)
	}
	else {
		next();
	}
}

module.exports.registerController = function(req,res){
	var user = {
		email:req.body.email,
		password:helper.hash_password(req.body.password),
		token:Token.getToken(req.body.email)
	};

    var sql = "INSERT INTO users SET ?";
	db.query(sql ,user,function (err, result) {
	  	if (err) throw err;
	    console.log("Database created");
	});
	res.redirect('/login');
}

module.exports.validateLogin = function(req,res,next){
	var user =req.body;
	// console.log(user.password != user.repassword);
	if(user.email.trim().length==0)
	{
		res.render("pages/login",{data : {error:"Ban chua nhap email"}});
		// console.log(data.error)
	}
	else if(user.password.trim().length==0)
	{
		res.render("pages/login",{data : {pw:"Ban chua nhap pw"}});
		// console.log(data.error)
	}
	else {
		next();
	}
}

module.exports.loginController = function(req,res){
	var email = req.body.email;
    var sql = "SELECT * FROM users WHERE ?"
	db.query(sql ,{email:email} ,function (err, result) {
	  	if (err) {
	  		console.log(err);
	  	}
	  	else{
	  		if(result[0]){
	  			var status = helper.comparePassword(req.body.password,result[0].password);
	  			if(status){
	  				res.cookie('user_id',Token.getToken(req.body.email));
	  				res.redirect('/');
	  			}
	  			else{
	  				res.render('pages/login',{data:{error:'dang nhap that bai'}})
	  			}
	  		}
	  		else{
	  			res.render('pages/login',{data:{error:'tai khoan khong ton tai'}})
	  		}
	  	}
	});
}

// module.exports.test = function(req,res){
// 	var cookie = helper.hash_password('tuantran');
// 	res.cookie('user_id',cookie);
// 	res.send("hello");
// 	var status = helper.comparePassword('tuantran',cookie);
// 	console.log(status);
// };