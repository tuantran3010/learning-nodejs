var test = require('./mysql.js');

test.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "CREATE TABLE test (name VARCHAR(255), address VARCHAR(255))";
  test.query(sql, function (err, result) {
  	if (err) throw err;
    console.log("Database created");
  });
});