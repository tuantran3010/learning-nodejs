var q = require("q");

function show(err,data){
	var defer = q.defer();
	if(err){
		defer.reject(err);
	}
	else{
		defer.resolve(data);
	}

	return defer.promise
}

show(false,"datatest")
.then(function(data){
	console.log(data);
	var data2 = "datatest ver 2";
	return data2;
})
.then(function(data2){
	console.log(data2);
})
.catch(function(err){
	console.log(err);
})