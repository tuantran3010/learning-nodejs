var express = require("express");
var userController = require("./../controller/blogController/userController");
var authMiddleware = require("./../controller/blogController/authMiddleware");
var postsController = require("./../controller/blogController/postsController");
var router = express.Router();

router.use("/admin",require(__dirname+"/admin"));
router.use("/home",require(__dirname+"/home"));

// router.get("/",authMiddleware.auth,function(req,res){
// 	res.render("home");
// });
router.get("/login",function(req,res){
	res.render("pages/login",{data:{}});
});
router.post("/login",userController.validateLogin,userController.loginController);

router.get("/register",function(req,res){
	res.render("pages/register",{data:{}});
});
router.post("/register",userController.validateRegister,userController.registerController);

router.get("/logout",function(req,res){
	res.clearCookie("user_id");
	res.redirect('/login');
});

router.get("/posts",postsController.show);
router.get("/createpost",function(req,res){
	res.render("pages/create_new_post");
});
router.post("/createpost",postsController.create);
router.get("/post/edit/:id",postsController.edit);

router.put("/post/update",postsController.update);

router.get("/chat",function(req,res){
	res.render("chat");
});
// router.get("/posts/abc/edit",function(req,res){
// 	res.render("home");
// });
// router.get("/cookie",userController.test);

module.exports = router;